---
layout: component-category
group: usability
permalink: /usability/index.html
url: /usability/index.html

title: Usability
description: Utilities are CSS helper classes that apply a single rule to an element. They should be used to accomplish specific styling needs, and should be used sparingly, especially when modifying the default styling of a Style Guide Guide component.


---
