---
layout: component-category
group: accessibility
permalink: /accessibility/index.html
url: /accessibility/index.html

title: Accessibility
description: Utilities are CSS helper classes that apply a single rule to an element. They should be used to accomplish specific styling needs, and should be used sparingly, especially when modifying the default styling of a Style Guide Guide component.


---
