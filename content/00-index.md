---
layout: component-category
group: content
permalink: /content/index.html
url: /content/index.html

title: Content
description: Utilities are CSS helper classes that apply a single rule to an element. They should be used to accomplish specific styling needs, and should be used sparingly, especially when modifying the default styling of a Style Guide Guide component.


---
