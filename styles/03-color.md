---
layout: page
type: detail
title: Color
group: styles
permalink: /regions/color.html
description: Color styles define how color is used in the design system.

---

For inspiration, check out [Material Design's color styles page](https://material.io/foundations/style/color.html).
