---
layout: page
type: detail
title: Motion
group: styles
permalink: /regions/motion.html
description: How the style guide uses motion to enhance the user experience.

---

For inspiration, check out [Material Design's thorough motion guidelines](https://material.io/foundations/motion/material-motion.html#material-motion-why-does-motion-matter), as well as all of [Val Head](http://valhead.com/)'s fantastic resources around UI animation.
