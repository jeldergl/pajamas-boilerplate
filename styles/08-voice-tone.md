---
layout: page
type: detail
title: Voice and tone
group: styles
permalink: /regions/voice-tone.html
description: Capturing the personality and authority of the brand in words

---

For inspiration, check out MailChimp's fantastic [Voice and Tone](http://voiceandtone.com/) website.
