---
layout: page
type: detail
title: Typography
group: styles
permalink: /regions/typography.html
description: Typography style description

---

For inspiration, check out [Material Design's typography styles](https://material.io/foundations/style/typography.html#typography-styles)
